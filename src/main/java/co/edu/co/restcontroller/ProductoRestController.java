package co.edu.co.restcontroller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.co.domain.Producto;
import co.edu.co.exceptions.Excepcion;
import co.edu.co.mensajes.Mensajes;
import co.edu.co.util.validator.ProductoValidator;

@RestController
@RequestMapping("/productos")
@CrossOrigin("http://localhost:4200")
public class ProductoRestController {
	
	@Autowired
	private ProductoValidator productoValidado;
	
	private Map<String,Object> response = new HashMap<>();
	private HttpStatus status;

	@GetMapping("/listar")
	public List<Producto> listAll(){
		return productoValidado.listaProductos();		
	}
	@GetMapping("/producto/{id}")
	public Producto findById(@PathVariable int id) throws Excepcion {
		Producto retorno;
		if(productoValidado.consultaProducto(id) == null) {
			retorno = null;
		}
		else {
			retorno = productoValidado.consultaProducto(id);
		}
		return retorno;
	}
	
	@PostMapping("/insertar")
	public ResponseEntity<?> insert(@RequestBody Producto producto) throws Excepcion {
		if(productoValidado.consultaProducto(producto.getCodigoBarras()) != null) {
			response.put(Mensajes.ADVERTENCIA,Mensajes.PRODUCTOEXISTE+producto.getCodigoBarras());
			status = HttpStatus.BAD_REQUEST;
		}
		else {
			try {
				productoValidado.creacionProducto(producto);
				response.put(Mensajes.ADVERTENCIA,Mensajes.INGRESOEXITOSO);
				status = HttpStatus.CREATED;
			} catch (BindException e) {
				response.put("Causa", e.getObjectName().toString());
				response.put(Mensajes.ERROR,Mensajes.INGRESOFALLIDO);
				status = HttpStatus.BAD_REQUEST;
			}
		}
		return new ResponseEntity<>(response, status);
	}
	
	@DeleteMapping("/eliminar/{id}")
	public ResponseEntity<?> delete(@PathVariable int id) throws Excepcion {
		if(productoValidado.consultaProducto(id) == null) {
			response.put(Mensajes.ADVERTENCIA,Mensajes.PRODUCTONOEXISTE+id);
			status = HttpStatus.BAD_REQUEST;
		}
		else {
			productoValidado.eliminacionProducto(id);	
			response.put(Mensajes.RESPUESTA,Mensajes.INFOELIMINADA);
			status = HttpStatus.CREATED;
		}
		return new ResponseEntity<>(response, status);
	}
	
	@PutMapping("/actualizar")
	public ResponseEntity<?> actualizar(@RequestBody Producto producto) throws Excepcion {
		if(productoValidado.consultaProducto(producto.getCodigoBarras()) == null) {
			response.put(Mensajes.ADVERTENCIA,Mensajes.PRODUCTONOEXISTE+producto.getCodigoBarras());
			status = HttpStatus.BAD_REQUEST;
		}
		else {
			try {
				productoValidado.actualizacionProducto(producto);
			} catch (BindException e) {
				response.put(Mensajes.ADVERTENCIA,Mensajes.INFONOACTUALIZADA);
				status = HttpStatus.BAD_REQUEST;
			}
			response.put(Mensajes.RESPUESTA,Mensajes.INFOACTUALIZADA);
			status = HttpStatus.CREATED;
		}

		return new ResponseEntity<>(response, status);
	}

}
