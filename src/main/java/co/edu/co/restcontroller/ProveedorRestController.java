package co.edu.co.restcontroller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.edu.co.domain.Proveedor;
import co.edu.co.services.IProveedorService;

@RestController
@RequestMapping("/apiProveedor")
@CrossOrigin("http://localhost:4200")
public class ProveedorRestController {
		@Autowired
		private IProveedorService proveedorService;
		
		@GetMapping("/listar")
		public List<Proveedor> listAll(){
			return proveedorService.listAll();		
		}

		@GetMapping("/proveedor/{id}")
		public Proveedor findById(@PathVariable String id) {
			return proveedorService.findById(id);
		}

		@PostMapping("/insertar")
		public ResponseEntity<?> insert(@RequestBody Proveedor proveedor) {
				Map<String,Object> response = new HashMap<>();
				HttpStatus status;
				try {
					proveedorService.save(proveedor);
					response.put("Mensaje", "La información se ha insertado exitosamente");
					status = HttpStatus.CREATED;
				} catch (BindException e) {
					response.put("Causa", e.getObjectName().toString());
					response.put("Mensaje", "No se pudo insertar la información");
					status = HttpStatus.BAD_REQUEST;
				}
				return new ResponseEntity<>(response, status);
		}

		@DeleteMapping("eliminar/{id}")
		@ResponseStatus(HttpStatus.ACCEPTED)
		public void delete(@PathVariable String id) {
			proveedorService.deleteById(id);
		}
}
