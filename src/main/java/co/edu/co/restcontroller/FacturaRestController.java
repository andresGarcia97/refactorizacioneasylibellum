package co.edu.co.restcontroller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.edu.co.domain.Factura;
import co.edu.co.services.IFacturaService;

@RestController
@RequestMapping("/facturas")
@CrossOrigin("http://localhost:4200")
public class FacturaRestController {

	@Autowired
	private IFacturaService facturaService;
	
	@GetMapping("/listar")
	public List<Factura> listAll(){
		return facturaService.listAll();		
	}

	@GetMapping("/factura/{id}")
	public Factura findById(@PathVariable Long id) {
		return facturaService.findById(id);
	}

	@PostMapping("/insertar")
	public ResponseEntity<?> insert(@RequestBody Factura factura) {
			Map<String,Object> response = new HashMap<>();
			HttpStatus status;
			if (facturaService.findById(factura.getIdFactura()) == null) {
				try {
					facturaService.save(factura);
					response.put("Mensaje", "La informaci�n se ha insertado exitosamente");
					status = HttpStatus.CREATED;
				} catch (BindException e) {
					response.put("Causa", e.getObjectName().toString());
					response.put("Mensaje", "No se pudo insertar la informaci�n");
					status = HttpStatus.BAD_REQUEST;
				}
			}
			else {
				response.put("Mensaje", "ya existe esta factura");
				status = HttpStatus.CONFLICT;
			}
			
			return new ResponseEntity<>(response, status);
	}

	@DeleteMapping("eliminar/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void delete(@PathVariable Long id) {
		facturaService.deleteById(id);
	}

	public FacturaRestController() { super(); }
}
