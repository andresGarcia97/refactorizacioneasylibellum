package co.edu.co.restcontroller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.co.domain.Cliente;
import co.edu.co.exceptions.Excepcion;
import co.edu.co.mensajes.Mensajes;
import co.edu.co.services.IClienteService;
import lombok.NoArgsConstructor;

@RestController
@NoArgsConstructor
@RequestMapping("/clientes")
@CrossOrigin("http://localhost:4200")
public class ClienteRestController {

	@Autowired
	private IClienteService clienteServicio;

	private Map<String,Object> response = new HashMap<>();
	private HttpStatus status;

	@GetMapping("/listar")
	public List<Cliente> listAll(){
		return clienteServicio.listAll();		
	}
	
	@GetMapping("/cliente/{id}")
	public Cliente findById(@PathVariable String id) throws Excepcion {
		Cliente retorno;
		if(clienteServicio.findById(id) == null) {
			retorno = null;
		}
		else {
			retorno = clienteServicio.findById(id);
		}
		return retorno;
	}

	@PostMapping("/insertar")
	public ResponseEntity<?> insert(@RequestBody Cliente cliente) throws Excepcion {
		if(clienteServicio.findById(cliente.getIdentificacion()) != null) {
			response.put(Mensajes.ADVERTENCIA, Mensajes.CLIENTEEXISTE +cliente.getIdentificacion());
			status = HttpStatus.BAD_REQUEST;
		}
		else {
			try {
				clienteServicio.save(cliente);
				response.put(Mensajes.RESPUESTA,Mensajes.INGRESOEXITOSO);
				status = HttpStatus.CREATED;
			} catch (BindException e) {
				response.put("Causa", e.getObjectName().toString());
				response.put(Mensajes.ERROR,Mensajes.INGRESOFALLIDO);
				status = HttpStatus.BAD_REQUEST;
			}
		}
		return new ResponseEntity<>(response, status);
	}

	@DeleteMapping("/eliminar/{id}")
	public ResponseEntity<?> delete(@PathVariable String id) throws Excepcion {
		if(clienteServicio.findById(id) == null) {
			response.put(Mensajes.ADVERTENCIA,Mensajes.CLIENTENOEXISTE+id);
			status = HttpStatus.BAD_REQUEST;
		}
		else {
			clienteServicio.deleteById(id);		
			response.put(Mensajes.ADVERTENCIA,Mensajes.INFOELIMINADA);
			status = HttpStatus.CREATED;
		}
		return new ResponseEntity<>(response, status);
	}

	@PutMapping("/actualizar")
	public ResponseEntity<?> actualizar(@RequestBody Cliente cliente) throws Excepcion {
		if(clienteServicio.findById(cliente.getIdentificacion()) == null) {
			response.put(Mensajes.ADVERTENCIA,Mensajes.CLIENTENOEXISTE+cliente.getIdentificacion());
			status = HttpStatus.BAD_REQUEST;
		}
		else {
			try {
				clienteServicio.update(cliente);
			} catch (BindException e) {
				response.put(Mensajes.ADVERTENCIA,Mensajes.INFONOACTUALIZADA);
				status = HttpStatus.BAD_REQUEST;
			}
			response.put(Mensajes.ADVERTENCIA,Mensajes.INFOACTUALIZADA);
			status = HttpStatus.CREATED;
		}
		return new ResponseEntity<>(response, status);
	}
}