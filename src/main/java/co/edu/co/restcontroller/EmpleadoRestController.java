package co.edu.co.restcontroller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.edu.co.domain.Empleado;
import co.edu.co.exceptions.Excepcion;
import co.edu.co.services.IEmpleadoService;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@RestController
@RequestMapping("/empleados")
@CrossOrigin("http://localhost:4200")
public class EmpleadoRestController {

	@Autowired
	private IEmpleadoService empleadoService;

	private Map<String,Object> response = new HashMap<>();
	private HttpStatus status;
	private String advertencia = "Advertencia";

	@GetMapping("/listar")
	public List<Empleado> listAll(){
		return empleadoService.listAll();		
	}
	@GetMapping("/empleado/{id}")
	public Empleado findById(@PathVariable String id) throws Excepcion {
		Empleado retorno;
		if(empleadoService.findById(id) == null) {
			retorno = null;
		}
		else {
			retorno = empleadoService.findById(id);
		}
		return retorno;
	}

	@PostMapping("/insertar")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> insert(@RequestBody Empleado empleado) throws Excepcion {
		if(empleadoService.findById(empleado.getIdentificacion()) != null) {
			response.put(advertencia, "Ya existe un empleado con esta Identificaci�n: "+empleado.getIdentificacion());
			status = HttpStatus.BAD_REQUEST;
		}
		else {
			try {
				empleadoService.save(empleado);
				response.put(advertencia, "La información se ha insertado exitosamente");
				status = HttpStatus.CREATED;
			} catch (BindException e) {
				response.put("Causa", e.getObjectName().toString());
				response.put("Mensaje", "No se pudo insertar la informaci�n");
				status = HttpStatus.BAD_REQUEST;
			}
		}
		return new ResponseEntity<>(response, status);
	}

	@DeleteMapping("/eliminar/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public ResponseEntity<?> delete(@PathVariable String id) throws Excepcion {
		if(empleadoService.findById(id) == null) {
			response.put(advertencia, "No existe un cliente con la Identificaci�n: "+id);
			status = HttpStatus.BAD_REQUEST;
		}
		else {
			empleadoService.deleteById(id);		
			response.put(advertencia, "La informaci�n se ha eliminado exitosamente");
			status = HttpStatus.CREATED;
		}
		return new ResponseEntity<>(response, status);
	}

	@PutMapping("/actualizar")
	public ResponseEntity<?> actualizar(@RequestBody Empleado empleado) throws Excepcion {
		if(empleadoService.findById(empleado.getIdentificacion()) == null) {
			response.put(advertencia, "No existe un cliente con esta Identificaci�n, "+empleado.getIdentificacion());
			status = HttpStatus.BAD_REQUEST;
		}
		else {
			try {
				empleadoService.update(empleado);
			} catch (BindException e) {
				response.put(advertencia, "La informaci�n no se ha actualizado exitosamente");
				status = HttpStatus.BAD_REQUEST;
			}
			response.put(advertencia, "La informaci�n se ha actualizado exitosamente");
			status = HttpStatus.CREATED;
		}

		return new ResponseEntity<>(response, status);
	}

}
