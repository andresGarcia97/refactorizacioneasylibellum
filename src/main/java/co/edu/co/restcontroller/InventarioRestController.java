package co.edu.co.restcontroller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.edu.co.domain.Inventario;
import co.edu.co.services.IInventarioService;
 
@RestController
@RequestMapping("/apiinventario")
@CrossOrigin("http://localhost:4200")
public class InventarioRestController {
	
	@Autowired
	private IInventarioService inventarioService;
	
	@GetMapping("/listar")
	public List<Inventario> listAll(){
		return inventarioService.listAll();		
	}
	@GetMapping("/inventario/{id}")
	public Inventario findById(@PathVariable Long id) {
		return inventarioService.findById(id);
	}
	@PostMapping("/insertar")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> insert(@RequestBody Inventario inventario) {
		Map<String,Object> response = new HashMap<>();
		HttpStatus status;
		try {
			inventarioService.save(inventario);
			response.put("Mensaje", "La información se ha insertado exitosamente");
			status = HttpStatus.CREATED;
		} catch (BindException e) {
			response.put("Causa", e.getObjectName().toString());
			response.put("Mensaje", "No se pudo insertar la información");
			status = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<>(response, status);	
	}
	@DeleteMapping("/eliminar/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void delete(@PathVariable Long id) {
		inventarioService.deleteById(id);	
	}
}
