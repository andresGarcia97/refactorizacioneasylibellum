package co.edu.co.services;

import java.util.List;

import org.springframework.validation.BindException;

import co.edu.co.domain.Empleado;
import co.edu.co.exceptions.Excepcion;

public interface IEmpleadoService {
	
	List<Empleado> listAll();
	
	void save(Empleado empleado) throws BindException, Excepcion;

	Empleado findById(String id) throws Excepcion;

	void deleteById(String id) throws Excepcion;

	void update(Empleado empleado) throws BindException, Excepcion;

}
