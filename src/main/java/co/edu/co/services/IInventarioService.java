package co.edu.co.services;

import java.util.List;

import org.springframework.validation.BindException;

import co.edu.co.domain.Inventario;

public interface IInventarioService {
	
	List<Inventario> listAll();
	
	void save(Inventario inventario) throws BindException;

	Inventario findById(Long id);

	void deleteById(Long id);
	
}
