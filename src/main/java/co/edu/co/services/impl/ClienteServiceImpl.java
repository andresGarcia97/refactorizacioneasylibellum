package co.edu.co.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;

import co.edu.co.converter.ClienteConverter;
import co.edu.co.domain.Cliente;
import co.edu.co.exceptions.Excepcion;
import co.edu.co.repository.IClienteRepository;
import co.edu.co.services.IClienteService;
import co.edu.co.util.validator.ClienteValidator;
import lombok.NoArgsConstructor;

@Service
@NoArgsConstructor
public class ClienteServiceImpl implements IClienteService {

	@Autowired
	private IClienteRepository clienteRepository;
	@Autowired
	private ClienteConverter converter;
	@Autowired
	private ClienteValidator validator;

	@Override
	public List<Cliente> listAll() {
		return converter.entityToModel(clienteRepository.findAll());
	}

	@Override
	public void save(Cliente cliente) throws BindException, Excepcion {
		clienteRepository.save(converter.modelToEntity(validator.creacionCliente(cliente)));	
	}

	@Override
	public Cliente findById(String id) throws Excepcion {
		return validator.consultaCliente(converter.entityToModel(clienteRepository.findById(id).orElse(null)));
	}

	@Override
	public void deleteById(String id) throws Excepcion {
		clienteRepository.deleteById(validator.consultaCliente(converter.entityToModel(clienteRepository.findById(id).orElse(null))).getIdentificacion());
	}

	@Override
	public void update(Cliente cliente) throws BindException, Excepcion {
		clienteRepository.save(converter.modelToEntity(validator.actualizacionCliente(cliente)));
	}
}