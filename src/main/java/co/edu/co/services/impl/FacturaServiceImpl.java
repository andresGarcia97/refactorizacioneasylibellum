package co.edu.co.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;

import co.edu.co.converter.FacturaConverter;
import co.edu.co.domain.Factura;
import co.edu.co.repository.IFacturaRepository;
import co.edu.co.services.IFacturaService;

@Service
public class FacturaServiceImpl implements IFacturaService {

	@Autowired
	private IFacturaRepository facturaRepository;
	@Autowired
	private FacturaConverter converter;


	@Override
	public List<Factura> listAll() {
		return converter.entityToModel(facturaRepository.findAll());
	}

	@Override
	public void save(Factura factura) throws BindException {
		facturaRepository.save(converter.modelToEntity(factura));
	}

	@Override
	public Factura findById(Long id) {
		return converter.entityToModel(facturaRepository.findById(id).orElse(null));
	}

	@Override
	public void deleteById(Long id) {
		facturaRepository.deleteById(id);
	}

}
