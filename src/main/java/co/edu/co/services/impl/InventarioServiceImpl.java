package co.edu.co.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;

import co.edu.co.converter.InventarioConverter;
import co.edu.co.domain.Inventario;
import co.edu.co.repository.IInventarioRepository;
import co.edu.co.services.IInventarioService;

@Service
public class InventarioServiceImpl implements IInventarioService {

	@Autowired
	private IInventarioRepository inventarioRepository;
	@Autowired
	private InventarioConverter converter;
	@Override
	public List<Inventario> listAll() {
		return converter.entityToModel(inventarioRepository.findAll());
	}

	@Override
	public void save(Inventario inventario) throws BindException {
		inventarioRepository.save(converter.modelToEntity(inventario));
	}

	@Override
	public Inventario findById(Long id) {
		return converter.entityToModel(inventarioRepository.findById(id).orElse(null));
	}

	@Override
	public void deleteById(Long id) {
		inventarioRepository.deleteById(id);
	}

}
