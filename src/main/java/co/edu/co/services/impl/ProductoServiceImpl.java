package co.edu.co.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;

import co.edu.co.converter.ProductoConverter;
import co.edu.co.domain.Producto;
import co.edu.co.repository.IProductoRepository;
import co.edu.co.services.IProductoService;

@Service
public class ProductoServiceImpl implements IProductoService {
	@Autowired
	private IProductoRepository productoRepository;
	@Autowired
	private ProductoConverter productoConverter;
	@Override
	public List<Producto> listAll() {
		return productoConverter.entityToModel(productoRepository.findAll());
	}

	@Override
	public void save(Producto producto) throws BindException {
		productoRepository.save(productoConverter.modelToEntity(producto));

	}

	@Override
	public Producto findById(int id) {
		return productoConverter.entityToModel(productoRepository.findById(id).orElse(null));
	}

	@Override
	public void deleteById(Integer id) {
		productoRepository.deleteById(id);
	}

	@Override
	public void update(Producto producto) throws BindException {
		productoRepository.save(productoConverter.modelToEntity(producto));
	}

}
