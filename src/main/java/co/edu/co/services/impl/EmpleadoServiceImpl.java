package co.edu.co.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;

import co.edu.co.converter.EmpleadoConverter;
import co.edu.co.domain.Empleado;
import co.edu.co.exceptions.Excepcion;
import co.edu.co.repository.IEmpleadoRepository;
import co.edu.co.services.IEmpleadoService;
import co.edu.co.util.validator.EmpleadoValidator;
import lombok.NoArgsConstructor;

@Service
@NoArgsConstructor
public class EmpleadoServiceImpl implements IEmpleadoService {

	@Autowired
	private IEmpleadoRepository empleadoRepository;
	@Autowired
	private EmpleadoConverter converter;
	@Autowired
	private EmpleadoValidator validator;

	@Override
	public List<Empleado> listAll() {
		return converter.entityToModel(empleadoRepository.findAll());
	}

	@Override
	public void save(Empleado empleado) throws BindException, Excepcion {
		empleadoRepository.save(converter.modelToEntity(validator.creacionEmpleado(empleado)));
	}

	@Override
	public Empleado findById(String id) throws Excepcion {
		return validator.consultaEmpleado(converter.entityToModel(empleadoRepository.findById(id).orElse(null)));
	}

	@Override
	public void deleteById(String id) throws Excepcion {
		empleadoRepository.deleteById(validator.consultaEmpleado(converter.entityToModel(empleadoRepository.findById(id).orElse(null))).getIdentificacion());
	}

	@Override
	public void update(Empleado empleado) throws BindException, Excepcion {
		empleadoRepository.save(converter.modelToEntity(validator.actualizacionEmpleado(empleado)));
		
	}
}
