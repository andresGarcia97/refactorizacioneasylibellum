package co.edu.co.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;

import co.edu.co.domain.DetalleFactura;
import co.edu.co.repository.IDetalleFacturaRepository;
import co.edu.co.services.IDetalleFacturaService;
import co.edu.co.converter.DetalleFacturaConverter;
@Service
public class DetalleFacturaServiceImpl implements IDetalleFacturaService {
	
	@Autowired
	private IDetalleFacturaRepository detalleFacturaRepository;
	private DetalleFacturaConverter converter;
	@Override
	public List<DetalleFactura> listAll() {
		return converter.entityToModel(detalleFacturaRepository.findAll());
	}

	@Override
	public void save(DetalleFactura detalleFactura) throws BindException {
		detalleFacturaRepository.save(converter.modelToEntity(detalleFactura));
	}

	@Override
	public DetalleFactura findById(long id) {
		return converter.entityToModel(detalleFacturaRepository.findById(id).orElse(null));
	}

	@Override
	public void deleteById(Long id) {
		detalleFacturaRepository.deleteById(id);
	}

}
