package co.edu.co.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;

import co.edu.co.converter.ProveedorConverter;
import co.edu.co.domain.Proveedor;
import co.edu.co.repository.IProveedorRepository;
import co.edu.co.services.IProveedorService;

@Service
public class ProveedorServiceImpl implements IProveedorService {
	@Autowired
	private IProveedorRepository proveedorRepository;
	@Autowired
	private ProveedorConverter converter;
	@Override
	public List<Proveedor> listAll() {
		return converter.entityToModel(proveedorRepository.findAll());
	}

	@Override
	public void save(Proveedor proveedor) throws BindException {
		proveedorRepository.save(converter.modelToEntity(proveedor));
	}

	@Override
	public Proveedor findById(String id) {
		return converter.entityToModel(proveedorRepository.findById(id).orElse(null));
	}

	@Override
	public void deleteById(String id) {
		proveedorRepository.deleteById(id);
	}

}
