package co.edu.co.services;

import java.util.List;

import org.springframework.validation.BindException;

import co.edu.co.domain.Factura;

public interface IFacturaService {

	List <Factura> listAll();

	void save(Factura factura) throws BindException;

	Factura findById(Long id);

	void deleteById(Long id);

}
