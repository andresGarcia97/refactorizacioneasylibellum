package co.edu.co.services;

import java.util.List;

import org.springframework.validation.BindException;

import co.edu.co.domain.Producto;

public interface IProductoService {
	
	List<Producto> listAll();
	
	void save(Producto producto) throws BindException;

	Producto findById(int id);

	void deleteById(Integer id);

	void update(Producto producto) throws BindException;

}
