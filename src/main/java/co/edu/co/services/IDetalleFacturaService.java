package co.edu.co.services;

import java.util.List;

import org.springframework.validation.BindException;

import co.edu.co.domain.DetalleFactura;

public interface IDetalleFacturaService {
	
	List<DetalleFactura> listAll();
	
	void save(DetalleFactura detalleFactura) throws BindException;

	DetalleFactura findById(long id);

	void deleteById(Long id);

}
