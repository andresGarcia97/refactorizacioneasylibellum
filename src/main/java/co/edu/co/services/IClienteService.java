package co.edu.co.services;

import java.util.List;

import org.springframework.validation.BindException;

import co.edu.co.domain.Cliente;
import co.edu.co.exceptions.Excepcion;

public interface IClienteService {
	
	List<Cliente> listAll();
	
	void save(Cliente cliente) throws BindException, Excepcion;

	Cliente findById(String id) throws Excepcion;

	void deleteById(String id) throws Excepcion;

	void update(Cliente cliente) throws BindException, Excepcion;

}
