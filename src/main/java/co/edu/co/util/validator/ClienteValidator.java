package co.edu.co.util.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;

import co.edu.co.domain.Cliente;
import co.edu.co.entity.ClienteEntity;
import co.edu.co.exceptions.Excepcion;
import co.edu.co.util.UtilObjeto;
import co.edu.co.util.validator.generic.GenericValidator;

@Component
public class ClienteValidator extends GenericValidator<ClienteEntity>{

	public ClienteValidator() { super(); }

	private Cliente clienteNoNulo (Cliente cliente) {
		return UtilObjeto.utilObjeto().evitarNulos(cliente, new Cliente());
	}

	public Cliente consultaCliente (Cliente cliente) throws Excepcion {
		return clienteNoNulo(cliente).validarCreacionCliente(cliente);
	}

	public Cliente creacionCliente (Cliente cliente) throws Excepcion, BindException {
		return clienteNoNulo(cliente).validarCreacionCliente(cliente);
	}

	public Cliente actualizacionCliente (Cliente cliente) throws Excepcion, BindException {
		return clienteNoNulo(cliente).validarCreacionCliente(cliente);
	}	
}
