package co.edu.co.util.validator;

import org.springframework.stereotype.Component;

import co.edu.co.entity.DetalleFacturaEntity;
import co.edu.co.util.validator.generic.GenericValidator;

@Component
public class DetalleFacturaValidator extends GenericValidator<DetalleFacturaEntity>{

	public DetalleFacturaValidator() {
		super();
	}

}
