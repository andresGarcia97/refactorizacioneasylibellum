package co.edu.co.util.validator;

import org.springframework.stereotype.Component;

import co.edu.co.entity.InventarioEntity;
import co.edu.co.util.validator.generic.GenericValidator;

@Component
public class InventarioValidator extends GenericValidator<InventarioEntity>  {

	public InventarioValidator() {
		super();
	}

}
