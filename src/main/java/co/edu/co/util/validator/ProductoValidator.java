package co.edu.co.util.validator;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;

import co.edu.co.domain.Producto;
import co.edu.co.entity.ProductoEntity;
import co.edu.co.exceptions.Excepcion;
import co.edu.co.services.IProductoService;
import co.edu.co.util.validator.generic.GenericValidator;


@Component
public class ProductoValidator extends GenericValidator<ProductoEntity> {

	public ProductoValidator() {
		super();
	}
	
	@Autowired
	private IProductoService productoServicio;

	private Producto productoNoNulo (Producto producto) {
		return new Producto(producto.getCodigoBarras(),producto.getNombre(),producto.getMarca(),producto.getIva(),producto.getPrecioCompra(),producto.getPrecioVenta());
	}
	
	public Producto consultaProducto (int id) throws Excepcion {
		Producto retorno = productoServicio.findById(id);
		if(retorno != null) {
			productoNoNulo(retorno).validarCreacionProducto(retorno);
		}
		return retorno;
	}
	
	public void creacionProducto (Producto producto) throws Excepcion, BindException {
		productoNoNulo(producto).validarCreacionProducto(producto);
		productoServicio.save(producto);
	}
	
	public void actualizacionProducto (Producto producto) throws Excepcion, BindException {
		productoNoNulo(producto).validarCreacionProducto(producto);
		productoServicio.update(producto);
	}
	
	public Producto eliminacionProducto (Integer id) throws Excepcion {
		Producto retorno = productoServicio.findById(id);
		if(retorno != null) {
			productoNoNulo(retorno).validarCreacionProducto(retorno);
			productoServicio.deleteById(id);
		}
		return retorno;
	}
	
	public List<Producto> listaProductos(){
		return productoServicio.listAll();
	}	

}
