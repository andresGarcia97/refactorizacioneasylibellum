package co.edu.co.util.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;

import co.edu.co.domain.Empleado;
import co.edu.co.entity.EmpleadoEntity;
import co.edu.co.exceptions.Excepcion;
import co.edu.co.util.UtilObjeto;
import co.edu.co.util.validator.generic.GenericValidator;

@Component
public class EmpleadoValidator extends GenericValidator<EmpleadoEntity> {

	public EmpleadoValidator() {
		super();
	}

	private Empleado empleadoNoNulo (Empleado empleado) {
		return UtilObjeto.utilObjeto().evitarNulos(empleado, new Empleado());
	}

	public Empleado consultaEmpleado (Empleado empleado) throws Excepcion {	
		return empleadoNoNulo(empleado).validarCreacionEmpleado(empleado);
	}

	public Empleado creacionEmpleado (Empleado empleado) throws Excepcion, BindException {
		return empleadoNoNulo(empleado).validarCreacionEmpleado(empleado);
	}

	public Empleado actualizacionEmpleado (Empleado empleado) throws Excepcion, BindException {
		return empleadoNoNulo(empleado).validarCreacionEmpleado(empleado);
	}

}

