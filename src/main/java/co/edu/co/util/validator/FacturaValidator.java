package co.edu.co.util.validator;

import org.springframework.stereotype.Component;

import co.edu.co.entity.FacturaEntity;
import co.edu.co.util.validator.generic.GenericValidator;

@Component
public class FacturaValidator extends GenericValidator<FacturaEntity> {

	public FacturaValidator() {
		super();
	}

}
