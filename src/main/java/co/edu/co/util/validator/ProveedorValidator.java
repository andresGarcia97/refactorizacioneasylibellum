package co.edu.co.util.validator;

import org.springframework.stereotype.Component;

import co.edu.co.entity.ProveedorEntity;
import co.edu.co.util.validator.generic.GenericValidator;

@Component
public class ProveedorValidator extends GenericValidator<ProveedorEntity> {

	public ProveedorValidator() {
		super();
	}

}
