package co.edu.co.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "proveedor")
@Data
@NoArgsConstructor
public class ProveedorEntity {

	@Id
	@Column(name = "id")
	private String nit;

	@Column(name = "nombre", nullable = false, length = 50)
	private String nombre;

	@Column(name = "telefono", nullable = false, length = 20)
	private String telefono;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "proveedor_productos", joinColumns = @JoinColumn(name = "id_proveedor"), inverseJoinColumns = @JoinColumn(name = "id_producto"))
	private List<ProductoEntity> productos;

}
