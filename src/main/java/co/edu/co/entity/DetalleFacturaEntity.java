package co.edu.co.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "detalle_factura")
@Data
public class DetalleFacturaEntity {

	@Id
	@Column(name = "id_detalle")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(cascade = { CascadeType.PERSIST })
	@JoinColumn(name = "id_factura")
	private FacturaEntity factura;

	@Column(name = "cantidad_producto", nullable = false, scale = 0)
	private Long cantidadProducto;

	@OneToOne(cascade = { CascadeType.PERSIST })
	@JoinColumn(name = "id_producto")
	private ProductoEntity producto;

}
