package co.edu.co.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "inventario")
@Data
@NoArgsConstructor
public class InventarioEntity {

	// Ver Ids, parece que hay que generarlos o si no va a haber errores.
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	// Ver relacion con productos
	@OneToOne(cascade = { CascadeType.PERSIST })
	@JoinColumn(name = "id_producto")
	private ProductoEntity producto;

	@Column(name = "cantidad", nullable = false, scale = 0)
	private int cantidad;

	@Column(name = "fecha_ingreso_lote", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date fechaDeIngresoLote;

	@Column(name = "fecha_vencimiento_lote", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date fechaVencimientoLote;

}
