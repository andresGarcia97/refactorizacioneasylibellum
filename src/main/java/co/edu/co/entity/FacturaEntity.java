package co.edu.co.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "factura")
@Data
@NoArgsConstructor
public class FacturaEntity {

	@Id
	@Column(name = "id_factura")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idFactura;

	@Column(name = "fecha_venta", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaVenta;

	@OneToMany(mappedBy = "factura", cascade = CascadeType.ALL)
	private List<DetalleFacturaEntity> productosYCantidades;

	@Column(name = "valor_total", nullable = false, scale = 2)
	private Long valorTotal;

	@ManyToOne(cascade = { CascadeType.PERSIST })
	@JoinColumn(name = "id_cliente", nullable = false)
	private ClienteEntity cliente;

	@ManyToOne(cascade = { CascadeType.PERSIST})
	@JoinColumn(name = "id_empleado", nullable = false)
	private EmpleadoEntity empleado;

}
