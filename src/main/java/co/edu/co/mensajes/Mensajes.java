package co.edu.co.mensajes;

public class Mensajes {
	
	 private Mensajes() {
		super();
	}
	 
	 public static final String ADVERTENCIA = "Advertencia";
	 public static final String RESPUESTA = "Mensaje";
	 public static final String ERROR = "Error";
	 public static final String CLIENTEEXISTE = "YA existe un cliente con esta Identifición: ";
	 public static final String CLIENTENOEXISTE = "NO existe un cliente con la Identifición: ";
	 public static final String INGRESOEXITOSO = "La Información se ha insertado exitosamente";
	 public static final String INGRESOFALLIDO = "La Información no se ha podido insertar";
	 public static final String INFOELIMINADA = "La Información se ha eliminado exitosamente";
	 public static final String INFOACTUALIZADA = "La Información se ha actualizado exitosamente";
	 public static final String INFONOACTUALIZADA = "La Información NO se ha actualizado exitosamente";
	 public static final String PRODUCTOEXISTE = "Ya existe un producto con este codigo de barras: ";
	 public static final String PRODUCTONOEXISTE =  "No existe un producto con el codido de barras: ";

}
