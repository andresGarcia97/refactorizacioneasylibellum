package co.edu.co.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;

import co.edu.co.domain.Inventario;
import co.edu.co.entity.InventarioEntity;
import co.edu.co.util.validator.InventarioValidator;

@Component
public class InventarioConverter {
	@Autowired
	private InventarioValidator inventarioValidator;
	@Autowired
	private ProductoConverter productoConverter;

	public Inventario entityToModel(InventarioEntity entity) {
		return new Inventario(productoConverter.entityToModel(entity.getProducto()), entity.getCantidad(),
				entity.getFechaDeIngresoLote(), entity.getFechaVencimientoLote());
	}

	public InventarioEntity modelToEntity(Inventario inventario) throws BindException {
		InventarioEntity entity = new InventarioEntity();
		entity.setProducto(productoConverter.modelToEntity(inventario.getProducto()));
		entity.setCantidad(inventario.getCantidad());
		entity.setFechaDeIngresoLote(inventario.getFechaDeIngresoLote());
		entity.setFechaVencimientoLote(inventario.getFechaVencimientoLote());
		inventarioValidator.validate(entity);
		return entity;
	}

	public List<Inventario> entityToModel(List<InventarioEntity> inventariosEntity) {
		List<Inventario> inventarios = new ArrayList<Inventario>(inventariosEntity.size());
		inventariosEntity.forEach((entity) -> {
			inventarios.add(entityToModel(entity));
		});
		return inventarios;
	}

}
