package co.edu.co.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;

import co.edu.co.domain.Factura;
import co.edu.co.entity.FacturaEntity;
import co.edu.co.util.validator.FacturaValidator;

@Component
public class FacturaConverter {

	@Autowired
	ClienteConverter cliente; 
	@Autowired
	DetalleFacturaConverter detalle;
	@Autowired
	EmpleadoConverter empleado;
	@Autowired
	private FacturaValidator facturaValidator;
	
	public Factura entityToModel(FacturaEntity entity) {
		return new Factura(entity.getIdFactura(), entity.getFechaVenta(),
				detalle.entityToModel(entity.getProductosYCantidades()), entity.getValorTotal(),
				cliente.entityToModel(entity.getCliente()),
				empleado.entityToModel(entity.getEmpleado()));
	}

	public FacturaEntity modelToEntity(Factura factura) throws BindException {
		FacturaEntity entity = new FacturaEntity();
		entity.setIdFactura(factura.getIdFactura());
		entity.setFechaVenta(factura.getFechaVenta());
		entity.setProductosYCantidades(detalle.modelToentity(factura.getProductosYCantidades()));
		entity.setValorTotal(factura.getValorTotal());
		entity.setCliente(cliente.modelToEntity(factura.getCliente()));
		entity.setEmpleado(empleado.modelToEntity(factura.getEmpleado()));
		facturaValidator.validate(entity);
		return entity;
	}

	public List<Factura> entityToModel(List<FacturaEntity> entidadesFac) {
		List<Factura> facturas = new ArrayList<Factura>(entidadesFac.size());
		entidadesFac.forEach((entity) -> { facturas.add(entityToModel(entity)); }); 
		return facturas;
	}

}
