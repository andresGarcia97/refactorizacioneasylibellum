package co.edu.co.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;

import co.edu.co.domain.Cliente;
import co.edu.co.entity.ClienteEntity;
import co.edu.co.util.validator.ClienteValidator;

@Component
public class ClienteConverter {

	@Autowired
	private ClienteValidator clienteValidator;
	
	public Cliente entityToModel(ClienteEntity entity) {
		Cliente retorno;
		if(entity == null) {
			retorno = null;
		}
		else {
			retorno = new Cliente(entity.getIdentificacion(), entity.getNombre(), entity.getApellido(),entity.getTelefono(), 
					entity.getFechaNacimiento());
		}
		return retorno;
	}

	public ClienteEntity modelToEntity(Cliente cliente) throws BindException {
		ClienteEntity entidad = new ClienteEntity();
		entidad.setIdentificacion(cliente.getIdentificacion());
		entidad.setNombre(cliente.getNombre());
		entidad.setApellido(cliente.getApellido());
		entidad.setTelefono(cliente.getTelefono());
		entidad.setFechaNacimiento(cliente.getFechaNacimiento());
		clienteValidator.validate(entidad);
		return entidad;
	}

	public List<Cliente> entityToModel(List<ClienteEntity> clientesEntity) {
		List<Cliente> clientes = new ArrayList<Cliente>(clientesEntity.size());
		clientesEntity.forEach((entity) -> {
			clientes.add(entityToModel(entity));
		});
		return clientes;
	}
}