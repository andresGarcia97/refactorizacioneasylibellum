package co.edu.co.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;

import co.edu.co.domain.Empleado;
import co.edu.co.entity.EmpleadoEntity;
import co.edu.co.util.validator.EmpleadoValidator;
import lombok.NoArgsConstructor;

@Component
@NoArgsConstructor
public class EmpleadoConverter {
	@Autowired
	private EmpleadoValidator empleadoValidator;

	public Empleado entityToModel(EmpleadoEntity entity) {
		Empleado retorno;
		if(entity == null) {
			retorno = null;
		}
		else {
			retorno = new Empleado(entity.getIdentificacion(), entity.getUsuario(), entity.getContrasena(), entity.getNombre(), entity.getApellido(),
					entity.getRol(),entity.getTelefono(), entity.getSueldo(), entity.getFechaIngreso(), entity.getFechaIngreso());
		}
		return retorno;
	}

	public EmpleadoEntity modelToEntity(Empleado empleado) throws BindException {
		EmpleadoEntity entidad = new EmpleadoEntity();
		entidad.setIdentificacion(empleado.getIdentificacion());
		entidad.setUsuario(empleado.getUsuario());
		entidad.setContrasena(empleado.getContrasena());
		entidad.setNombre(empleado.getNombre());
		entidad.setApellido(empleado.getApellido());
		entidad.setRol(empleado.getRol());
		entidad.setTelefono(empleado.getTelefono());
		entidad.setSueldo(empleado.getSueldo());
		entidad.setFechaIngreso(empleado.getFechaIngreso());
		entidad.setFechaNacimiento(empleado.getFechaNacimiento());
		empleadoValidator.validate(entidad);
		return entidad;
	}

	public List<Empleado> entityToModel(List<EmpleadoEntity> entidades) {
		List<Empleado> empleados = new ArrayList<Empleado>(entidades.size());
		entidades.forEach((entity) -> { empleados.add(entityToModel(entity)); });
		return empleados;
	}
}
