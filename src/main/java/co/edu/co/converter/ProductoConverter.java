package co.edu.co.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;

import co.edu.co.domain.Producto;
import co.edu.co.entity.ProductoEntity;
import co.edu.co.util.validator.ProductoValidator;

@Component
public class ProductoConverter {
	
	@Autowired
	private ProductoValidator productoValidator;
	public Producto entityToModel(ProductoEntity productoEntity) {
		Producto retorno;
		if(productoEntity == null) {
			retorno = null;
		}
		else {
			retorno = new Producto(productoEntity.getCodigoBarras(),productoEntity.getNombre(),
					productoEntity.getMarca(),productoEntity.getIva(),productoEntity.getPrecioCompra(),productoEntity.getPrecioVenta());
		}
		return retorno;
	}

	public ProductoEntity modelToEntity(Producto producto) throws BindException {
		ProductoEntity productoEntity = new ProductoEntity();
		productoEntity.setCodigoBarras(producto.getCodigoBarras());
		productoEntity.setNombre(producto.getNombre());
		productoEntity.setMarca(producto.getMarca());
		productoEntity.setIva(producto.getIva());
		productoEntity.setPrecioCompra(producto.getPrecioCompra());
		productoEntity.setPrecioVenta(producto.getPrecioVenta());
		productoValidator.validate(productoEntity);
		return productoEntity;
	}
	
	public List<Producto> entityToModel(List<ProductoEntity> productosEntity) {
		List<Producto> productos = new ArrayList<Producto>(productosEntity.size());
		productosEntity.forEach((entity) -> {
			productos.add(entityToModel(entity));
		});
		return productos;
	}
	
	public List<ProductoEntity> modelToEntity(List<Producto> productos) {
		List<ProductoEntity> productosEntity = new ArrayList<ProductoEntity>(productos.size());
		productos.forEach((model) -> {
			try {
				productosEntity.add(modelToEntity(model));
			} catch (BindException e) {	
				e.printStackTrace();
			}
		});
		return productosEntity;
	}

}
