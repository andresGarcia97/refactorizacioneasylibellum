package co.edu.co.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;

import co.edu.co.domain.DetalleFactura;
import co.edu.co.entity.DetalleFacturaEntity;
import co.edu.co.entity.FacturaEntity;
import co.edu.co.util.validator.DetalleFacturaValidator;
import lombok.NoArgsConstructor;

@Component
@NoArgsConstructor
public class DetalleFacturaConverter {
	@Autowired
	ProductoConverter productoConverter;
	@Autowired
	private DetalleFacturaValidator detalleFacturaValidator;

	public DetalleFactura entityToModel(DetalleFacturaEntity entity) {
		return new DetalleFactura(entity.getFactura().getIdFactura(),entity.getCantidadProducto(),
				productoConverter.entityToModel(entity.getProducto()));
	}

	public DetalleFacturaEntity modelToEntity(DetalleFactura detalle) throws BindException {
		DetalleFacturaEntity entity = new DetalleFacturaEntity();
		entity.setFactura(new FacturaEntity());
		entity.getFactura().setIdFactura(detalle.getIdFactura());
		entity.setCantidadProducto(detalle.getCantidadProducto());
		entity.setProducto(productoConverter.modelToEntity(detalle.getProducto()));
		detalleFacturaValidator.validate(entity);
		return entity;
	}

	public List<DetalleFactura> entityToModel(List<DetalleFacturaEntity> detallesEntity) {
		List<DetalleFactura> detalles = new ArrayList<DetalleFactura>(detallesEntity.size());
		detallesEntity.forEach((entity) -> {
			detalles.add(entityToModel(entity));
		});
		return detalles;
	}

	public List<DetalleFacturaEntity> modelToentity(List<DetalleFactura> detallesFac) {
		List<DetalleFacturaEntity> detallesEnt = new ArrayList<DetalleFacturaEntity>(detallesFac.size());
		detallesFac.forEach((entity) -> {
			try {
				detallesEnt.add(modelToEntity(entity));
			} catch (BindException e) {
				e.printStackTrace();
			}
		});
		return detallesEnt;
	}

}
