package co.edu.co.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;

import co.edu.co.domain.Proveedor;
import co.edu.co.entity.ProveedorEntity;
import co.edu.co.util.validator.ProveedorValidator;

@Component
public class ProveedorConverter {
	@Autowired
	private ProveedorValidator validator;
	@Autowired
	private ProductoConverter productoConverter;

	public Proveedor entityToModel(ProveedorEntity proveedorEntity) {
		return new Proveedor(proveedorEntity.getNit(),proveedorEntity.getNombre(),
				proveedorEntity.getTelefono(), productoConverter.entityToModel(proveedorEntity.getProductos()));
	}

	public ProveedorEntity modelToEntity(Proveedor proveedor) throws BindException {
		ProveedorEntity proveedorEntity = new ProveedorEntity();
		proveedorEntity.setNit(proveedor.getNit());
		proveedorEntity.setNombre(proveedor.getNombre());
		proveedorEntity.setTelefono(proveedor.getTelefono());
		proveedorEntity.setProductos(productoConverter.modelToEntity(proveedor.getProductos()));
		validator.validate(proveedorEntity);
		return proveedorEntity;
	}

	public List<Proveedor> entityToModel(List<ProveedorEntity> proveedorEntity) {
		List<Proveedor> proveedores = new ArrayList<Proveedor>(proveedorEntity.size());
		proveedorEntity.forEach((entity) -> {
			proveedores.add(entityToModel(entity));
		});
		return proveedores;
	}

	public List<ProveedorEntity> modelToEntity(List<Proveedor> proveedores) {
		List<ProveedorEntity> proveedorEntity = new ArrayList<ProveedorEntity>(proveedores.size());
		proveedores.forEach((model) -> {
			try {
				proveedorEntity.add(modelToEntity(model));
			} catch (BindException e) {	
				e.printStackTrace();
			}
		});
		return proveedorEntity;
	}
}
